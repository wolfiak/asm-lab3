.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
		cout		   dd ?
		cin			   dd ?

		source		   dd 10 dup(?)
		destination	   db 10 dup(?)
		tekst          db "Wprowadz liczbe %i :",0
		rozmiar		   dd $ - tekst
		bufor		   dd 10 DUP(?)
		buforZ		   dd 10 DUP(?)

		lznakow		   dw  0
		lznakowZ	   dw  0
		liczba		   dd ?
		licznik		   dd 0
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	mov EAX, 1
	mov liczba,EAX
	mov ECX, 10
	cld
	mov EDI, OFFSET source
	Petla:
		push ECX
		push liczba
		push EDI
		invoke wsprintfA, OFFSET bufor, OFFSET tekst, liczba
	
		invoke WriteConsoleA, cout, OFFSET bufor, rozmiar, OFFSET lznakow, 0
		invoke ReadConsoleA, cin, OFFSET buforZ, 10, OFFSET lznakowZ, 0
		
		mov EAX, buforZ
		
		pop EDI
		stosd
		
		
		pop liczba
		mov EAX, liczba
		add EAX, 1d
		mov liczba,EAX

		
		pop ECX
		
	loop Petla

	mov ECX, LENGTHOF source
	mov ESI, OFFSET source
	wyswietlanie:
	push ECX
	mov EAX,0
	
	lodsb

	pop ECX
	loop wyswietlanie


	invoke ExitProcess, 0

main endp

END